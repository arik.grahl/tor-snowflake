FROM golang:1.19.2-alpine AS builder

COPY . /usr/src/app

WORKDIR /usr/src/app/proxy

RUN CGO_ENABLED=0 go build -o proxy -ldflags '-extldflags "-static" -w -s'  .

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/src/app/proxy/proxy /bin/proxy

USER 1000

ENTRYPOINT ["/bin/proxy"]
